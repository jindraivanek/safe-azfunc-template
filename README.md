# SAFE Template with Azure Functions

This template was created from [SAFE Template](https://safe-stack.github.io/docs/template-overview/) and altered to use Azure Functions as Server.

You can see it live at: https://jindraivanek.gitlab.io/safe-azfunc-template/

### Main differences from SAFE-template
* included full-build Dockerfile with deploying capabilities
* included auto-deploy CI for GitLab
* watch mode for Server is supported by using `dotnet watch` on `Server.Local` project that call `func` command (see issue: https://github.com/Azure/azure-functions-core-tools/issues/644)

## Install pre-requisites

You'll need to install the following pre-requisites in order to build SAFE applications

* The [.NET Core SDK](https://www.microsoft.com/net/download)
* [FAKE 5](https://fake.build/) installed as a [global tool](https://fake.build/fake-gettingstarted.html#Install-FAKE)
* The [Yarn](https://yarnpkg.com/lang/en/docs/install/) package manager (you an also use `npm` but the usage of `yarn` is encouraged).
* [Node LTS](https://nodejs.org/en/download/) installed for the front end components.
* If you're running on OSX or Linux, you'll also need to install [Mono](https://www.mono-project.com/docs/getting-started/install/).
* Install Azure Functions Core Tool: `npm install -g azure-functions-core-tools`.
* For deploying to Azure install `az`: https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest

## Work with the application

To concurrently run the server and the client components use the following command (in watch mode):

```bash
fake build -t Run
```

## Deploying

Name and URL of Azure Functions App is in `src/Shared/Config.fs` file.

Run `fake build -t bundle` to create Client distribution ready to deploy that use Azure Functions App for remote calls.

Run `fake build -t DeployServer` to build and publish Server to Azure Functions. Azure Functions App with name defined in `src/Shared/Config.fs` must exists in your Azure account. You need to be logged in to Azure via `az` CLI beforehand.

### Azure Functions configuration
* In CORS you must allow URL where Client pages will be deployed.

### CI
`deploy.Dockerfile` Dockerfile build Client into `/build/public` dir, and deploy Server into Azure Functions. For logging in it uses [Service Principals](https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli?view=azure-cli-latest). Credentials for Service Principal account need to be provided in `AZ_USER`, `AZ_PASS`, `AZ_TENANT` build args. You can use this dockerfile locally with this command:

```
docker build -f deploy.Dockerfile --build-arg AZ_DEPLOY=1 --build-arg AZ_PASS=<password> --build-arg AZ_USER=<username> --build-arg AZ_TENANT=<tenant> .
```

Without `build-args`, Dockerfile do unly build, dont try to deploy:

```
docker build -f deploy.Dockerfile .
```

Gitlab CI is configured (via `.gitlab_ci.yml`) to deploy on chages to `master` branch, client is deployed to GitLab Pages, server is deployed to Azure only when there is some changes in `src/Server` dir.

### Summary of manual steps
Steps needed to deploy this on your Azure using GitLab CI:
* Create new Azure Functions App, edit `src/Shared/Config.fs` with its name and URL
* In FunctionApp CORS rules, add new entry with `https://<gitlab_username>.gitlab.io`
* With `az` tool create new Service Principal with `az ad sp create-for-rbac --name ServicePrincipalName`, save credentials into GitLab CI variables with names `SECRET_AZ_USER`, `SECRET_AZ_PASS`, `SECRET_AZ_TENANT`. (You can reuse same Service Principal for other Azure Functions Apps on your account.)


## SAFE Stack Documentation

You will find more documentation about the used F# components at the following places:

* [Giraffe](https://github.com/giraffe-fsharp/Giraffe/blob/master/DOCUMENTATION.md)
* [Fable](https://fable.io/docs/)
* [Elmish](https://elmish.github.io/elmish/)
* [Fable.Remoting](https://zaid-ajaj.github.io/Fable.Remoting/)
* [Fulma](https://fulma.github.io/Fulma/)

If you want to know more about the full Azure Stack and all of it's components (including Azure) visit the official [SAFE documentation](https://safe-stack.github.io/docs/).

## Troubleshooting

* **fake not found** - If you fail to execute `fake` from command line after installing it as a global tool, you might need to add it to your `PATH` manually: (e.g. `export PATH="$HOME/.dotnet/tools:$PATH"` on unix) - [related GitHub issue](https://github.com/dotnet/cli/issues/9321)
