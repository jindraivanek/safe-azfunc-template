module Server.Local

// Only puropose of this project is to allow dotnet watch mode for Server.

open System.Diagnostics

[<EntryPoint>]
let main argv =
    if Seq.length argv < 2 then printfn "USAGE: working_dir file_name arguments"
    let d = argv.[0]
    let p = ProcessStartInfo()
    p.WorkingDirectory <- d
    p.FileName <- argv.[1]
    p.Arguments <- argv |> Seq.skip 2 |> String.concat " "
    printfn "Running %s %s in directory %s" p.FileName p.Arguments p.WorkingDirectory
    Process.Start(p).WaitForExit()
    0