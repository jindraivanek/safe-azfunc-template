FROM jindraivanek/fable-azure-build
SHELL ["/bin/bash", "-c"]

WORKDIR /build

# Package lock files are copied independently and their respective package
# manager are executed after.
#
# This is voluntary as docker will cache images and only re-create them if
# the already-copied files have changed, by doing that as long as no package
# is installed or updated we keep the cached container and don't need to
# re-download.

# Initialize node_modules
COPY package.json yarn.lock ./
RUN yarn install

# Initialize paket packages
COPY paket.dependencies paket.lock ./
COPY .paket .paket
RUN paket restore

# Copy everything else and run the build
COPY . ./
RUN dotnet run -p tests
